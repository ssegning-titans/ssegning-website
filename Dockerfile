# Prod image
FROM node:16

LABEL maintainer="Segning <me@segning.com>"

ENV NODE_ENV=production

WORKDIR /app

COPY ./ .

EXPOSE 3000

ENTRYPOINT ["yarn", "start"]
