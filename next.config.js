// This file sets a custom webpack configuration to use your Next.js app
// with Sentry.
// https://nextjs.org/docs/api-reference/next.config.js/introduction
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

const {withSentryConfig} = require('@sentry/nextjs');
const {createSecureHeaders} = require('next-secure-headers');

const sources = [
    "'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    'https://ssegning.com',
    'https://*.ssegning.com',
    'https://*.ingest.sentry.io',
    'https://cdn.jsdelivr.net',
];
if (process.env.NODE_ENV !== 'production') {
    sources.push('http://localhost:8080');
}

/** @type {import('next').NextConfig} */
const moduleExports = {
    webpack: (config) => {
        config.module.rules.push({
            test: /\.md$/,
            use: 'raw-loader',
        });
        return config;
    },
    // Your existing module.exports
    productionBrowserSourceMaps: true,
    reactStrictMode: true,
    images: {
        domains: [
            'ssegning.com',
            '*.ssegning.com',
            process.env.NODE_ENV !== 'production' && 'localhost',
        ].filter((i) => !!i),
        imageSizes: [16, 24, 32, 48, 64, 96, 128, 256, 384],
    },
    async headers() {
        return [
            {
                source: '/(.*)',
                headers: createSecureHeaders({
                    contentSecurityPolicy: {
                        directives: {
                            defaultSrc: sources,
                            imgSrc: [...sources, 'data:', 'blob:'],
                        },
                    },
                    forceHTTPSRedirect: [
                        true,
                        {maxAge: 60 * 60 * 24 * 4, includeSubDomains: true},
                    ],
                    referrerPolicy: 'strict-origin-when-cross-origin',
                    frameGuard: 'sameorigin',
                    noopen: 'noopen',
                    nosniff: 'nosniff',
                    xssProtection: 'sanitize',
                }),
            },
        ];
    },

};

const sentryWebpackPluginOptions = {
    // Additional config options for the Sentry Webpack plugin. Keep in mind that
    // the following options are set automatically, and overriding them is not
    // recommended:
    //   release, url, org, project, authToken, configFile, stripPrefix,
    //   urlPrefix, include, ignore

    silent: true // Suppresses all logs
    // For all available options, see:
    // https://github.com/getsentry/sentry-webpack-plugin#options.
};

// Make sure adding Sentry options is the last code to run before exporting, to
// ensure that your source maps include changes from all other Webpack plugins
module.exports = withSentryConfig(moduleExports, sentryWebpackPluginOptions);
