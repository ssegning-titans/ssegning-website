export interface Licence {
    label: string;
    url: string;
    title: string;
}

// TODO
export const licences: Licence[] = [
    {
        label: 'Digital money icons created by GOWI - Flaticon',
        url: 'https://www.flaticon.com/free-icons/digital-money',
        title: 'digital money icons',
    },
    {
        label: 'Document icons created by vectorsmarket15 - Flaticon',
        url: 'https://www.flaticon.com/free-icons/document',
        title: 'document icons',
    },
    {
        label: 'Certificate icons created by Freepik - Flaticon',
        url: 'https://www.flaticon.com/free-icons/certificate',
        title: 'certificate icons',
    },
];
