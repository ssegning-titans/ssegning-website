export interface Assets {
    name: string;
    description: string;
    files: AssetsFile[];
}


export interface AssetsFile {
    url: string;
    name: string;
    size: number;
    mimeType: string;
}

// TODO
export const assets: Assets[] = [
    {
        name: 'SSchool Brand',
        description: 'Here are the logo and assets for the organisation SSchool.',
        files: [
            {
                mimeType: 'image/png',
                url: '',
                size: 0,
                name: 'SSchool Logo.png'
            },
            {
                mimeType: 'application/zip',
                url: '',
                size: 0,
                name: 'Brand.zip'
            },
        ]
    },
    {
        name: 'SSegning Brand',
        description: 'You may also need to have a Logo of SSegning for a flyer? Here it is.',
        files: [
            {
                mimeType: 'image/png',
                url: '',
                size: 0,
                name: 'SSegning Logo.png'
            },
            {
                mimeType: 'application/zip',
                url: '',
                size: 0,
                name: 'Brand.zip'
            },
        ]
    },
    {
        name: 'Djaller Brand',
        description: 'You have a new tontine created and you need to have a fully sized logo? Here it is.',
        files: [
            {
                mimeType: 'image/png',
                url: '',
                size: 0,
                name: 'Djaller Logo.png'
            },
            {
                mimeType: 'application/zip',
                url: '',
                size: 0,
                name: 'Brand.zip'
            },
        ]
    },
    {
        name: '99Travl Brand',
        description: 'Need a logo for your next travel using 99Travl? Here it is.',
        files: [
            {
                mimeType: 'image/png',
                url: '',
                size: 0,
                name: '99Travl Logo.png'
            },
            {
                mimeType: 'application/zip',
                url: '',
                size: 0,
                name: 'Brand.zip'
            },
        ]
    }
];
