import {DefaultSeoProps} from "next-seo";

const SEO: DefaultSeoProps = {
    openGraph: {
        type: 'website',
        locale: 'en_EN',
        url: 'https://ssegning.com/',
        site_name: 'SSegning',
        title: 'SSegning IT Solutions',
        images: [
            {
                url: 'https://ssegning.com/ssegning.jpg',
                width: 3509,
                height: 2481,
                alt: 'SSegning Main Alt',
                type: 'image/jpeg',
            },
            {
                url: 'https://ssegning.com/ssegning-full.png',
                width: 3509,
                height: 2481,
                alt: 'SSegning',
                type: 'image/png',
            },
            {url: 'https://ssegning.com/logo.png'},
            {url: 'https://ssegning.com/logo-inv.png'},
        ],
        description: 'We\'re a team of developers, managers, designers and hard workers. We love creating new things, from scratch or not. So we invest them many time in counselling our customers for them to take control over their fears and to benefit from all their advantages.'
    },
    twitter: {
        handle: '@selast',
        site: '@ssegning',
        cardType: 'summary_large_image',
    },
    titleTemplate: '%s | SSegning',
    defaultTitle: 'SSegning',
    description: 'We provide personalized IT Solutions',
    additionalMetaTags: [
        {
            name: 'viewport',
            content: 'initial-scale=1, width=device-width'
        }
    ],
};

export default SEO;
