import {memo} from "react";
import {Box, Button, Container, Divider, Grid, Link, Typography} from "@mui/material";
import LL from 'next/link';
import Image from "next/image";

const footers = [
    {
        title: 'Resources',
        description: [
            {
                n: 'Assets',
                l: '/resources/assets',
            },
            {
                n: 'Licences',
                l: '/resources/licences',
            },
        ],
    },
    {
        title: 'Legal',
        description: [
            {
                n: 'Privacy policy',
                l: '/legal/privacy-policy',
            },
            {
                n: 'Terms of use',
                l: '/legal/terms-of-use',
            },
        ],
    },
];

const external = [
    {
        n: 'Djaller',
        l: 'https://djaller.com',
    },
    {
        n: '99Travl',
        l: 'https://99Travl.com',
    },
    {
        n: 'SSchool',
        l: 'https://sschool.app',
    },
    {
        n: 'AVEK',
        l: 'https://shop.ssegning.com',
    },
];


export interface FooterProps {
    hideContact?: true;
}

export const Footer = memo(({hideContact}: FooterProps) => {
    return (
        <>
            {!hideContact && (
                <Box id='contact-block' bgcolor='text.primary' color='background.paper' py='2rem'>
                    <Container>
                        <Grid container spacing={8} justifyContent="space-between" alignItems='center'>
                            <Grid item xs={12} md={5}>
                                <Typography variant='h3' component='h2' pb='1rem'>
                                    Contact us!
                                </Typography>
                                <Typography variant='subtitle1' component='p' pb='1rem'>
                                    Yes! Contact us right with your idea. If your requirement is different from what is
                                    shown here, then we my still be able to help you
                                </Typography>
                                <LL href='/contact' passHref>
                                    <Button size="large" variant="contained">
                                        <Box component='span' px='1rem'>Contact us</Box>
                                    </Button>
                                </LL>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            )}

            <Container
                component="footer"
                sx={{
                    py: '1rem',
                }}
            >
                <Grid py='2rem' container spacing={4} justifyContent="space-evenly">
                    <Grid item xs={12} sm={6}>
                        <Image src='/ssegning-full.png' width={280} height={80} alt='SSegning Logo'/>
                    </Grid>

                    {footers.map((footer) => (
                        <Grid item xs={6} sm={3} key={footer.title}>
                            <Typography variant="h6" color="text.primary" gutterBottom>
                                {footer.title}
                            </Typography>
                            <ul>
                                {footer.description.map((item) => (
                                    <li key={item.n}>
                                        <LL href={item.l} passHref>
                                            <Link underline='none' variant="subtitle1" color="text.secondary">
                                                {item.n}
                                            </Link>
                                        </LL>
                                    </li>
                                ))}
                            </ul>
                        </Grid>
                    ))}
                </Grid>

                <Divider/>

                <Box display={{md: 'flex'}} pt='1rem' justifyContent='space-between' alignItems='center'>
                    <Box>
                        {external.map((value) => (
                            <Link target='blank' rel='noopener' underline='none' href={value.l} key={value.l}
                                  variant="subtitle1"
                                  color="text.secondary" mr='1rem'>
                                {value.n}
                            </Link>
                        ))}
                    </Box>
                    <Typography variant="body1" color="text.secondary">
                        {'Copyright © '}
                        <Link color="inherit" href="/">
                            SSegning
                        </Link>{' '}
                        2022
                        {'.'}
                    </Typography>
                </Box>
            </Container>
        </>
    );
});
