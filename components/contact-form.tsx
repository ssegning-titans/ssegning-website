import React, {FormEvent, memo, useCallback, useRef, useState} from "react";
import {Box, Button, Grid, Slide, Snackbar, TextField} from "@mui/material";
import {Send} from "react-feather";
import {useRouter} from "next/router";

export interface ContactFormProps {
}

export const ContactForm = memo((props: ContactFormProps) => {
    const formRef = useRef<HTMLFormElement>();
    const {query} = useRouter();
    const [snackBarMessage, setSnackBarMessage] = useState<string>();
    const [snackBarOpen, setSnackBarOpen] = useState<boolean>();
    const [sending, setSending] = useState<boolean>();

    const snackBarHandleClose = useCallback(() => {
        setSnackBarOpen(false);
    }, [setSnackBarOpen]);

    const openSnackBar = useCallback((message: string) => {
        setSnackBarMessage(message);
        setSnackBarOpen(true);
    }, [setSnackBarMessage, setSnackBarOpen]);

    const handleSubmit = useCallback(async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (sending) {
            return;
        }

        setSending(true);

        try {
            const data = new FormData(event.currentTarget);

            const response = await fetch('/api/contact', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(Object.fromEntries(data))
            });

            const responseData = await response.json();
            if (response.status != 200) {
                openSnackBar(responseData.error);
            } else {
                console.log(responseData);
                openSnackBar('Sent successfully');
                formRef.current?.reset();
            }
        } catch (e) {
            openSnackBar((e as Error).message);
        }

        setSending(false);
    }, [openSnackBar, sending]);

    return (
        <>
            <Box component="form" ref={formRef} onSubmit={handleSubmit} sx={{mt: 3}}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            disabled={sending}
                            autoComplete="given-name"
                            name="firstName"
                            required
                            fullWidth
                            id="firstName"
                            label="First Name"
                            autoFocus
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            disabled={sending}
                            required
                            fullWidth
                            id="lastName"
                            label="Last Name"
                            name="lastName"
                            autoComplete="family-name"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            disabled={sending}
                            fullWidth
                            required
                            id="organisation"
                            label="Organisation"
                            name="organisation"
                            autoComplete="organization"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            disabled={sending}
                            fullWidth
                            required
                            id="organisation-title"
                            label="Your post"
                            name="organisationTitle"
                            autoComplete="organization-title"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            disabled={sending}
                            required
                            fullWidth
                            type='email'
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            defaultValue={query['email']}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            disabled={sending}
                            required
                            fullWidth
                            multiline
                            name="message"
                            label="Message"
                            id="message"
                            rows={4}
                        />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
                    startIcon={<Send strokeWidth={1}/>}
                    size='large'
                >
                    Send
                </Button>
            </Box>
            <Snackbar
                open={snackBarOpen}
                onClose={snackBarHandleClose}
                TransitionComponent={Slide}
                message={snackBarMessage}
            />
        </>
    );
});
