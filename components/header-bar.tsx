import {ReactNode} from "react";
import {AppBar, Box, Container, useScrollTrigger} from "@mui/material";

export interface HeaderBarProps {
    children: ReactNode | ReactNode[];
}

export function HeaderBar({children}: HeaderBarProps) {
    const trigger = useScrollTrigger({disableHysteresis: true});
    return (
        <AppBar
            color={trigger ? 'inherit' : 'transparent'}
            elevation={trigger ? 2 : 0}
            position='sticky'
            sx={{
                transition: '250ms ease',
            }}
        >
            <Container component={Box}>
                {children}
            </Container>
        </AppBar>
    );
}
