import {Box, IconButton, InputAdornment, TextField, Typography} from "@mui/material";
import {ArrowDown, AtSign, Send} from "react-feather";
import {FormEvent, useCallback, useState} from "react";
import {useRouter} from "next/router";

export interface ContactEmailInputProps {

}

export function ContactEmailInput({}: ContactEmailInputProps) {
    const [value, setEmail] = useState<string>();
    const router = useRouter();
    const gotoContact = useCallback((event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (!!value) {
            router.push('/contact?email=' + value)
        }
    }, [value, router]);

    return (
        <Box component='form' onSubmit={gotoContact}>
            <Box pb='1rem' alignItems='center' display='flex' flexWrap='wrap' component='label'
                 htmlFor='contact-email-intent'>
                <Typography variant='body1' component='p'>
                    Need a Service? Write us now!
                </Typography>
                <ArrowDown strokeWidth={1}/>
            </Box>

            <TextField
                fullWidth
                color='primary'
                sx={{bgcolor: 'white'}}
                onChange={event => setEmail(event.target.value)}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <AtSign strokeWidth={1}/>
                        </InputAdornment>
                    ),
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton type='submit' edge="end" color='primary'
                                        aria-label="navigate to contact page with email">
                                <Send strokeWidth={1}/>
                            </IconButton>
                        </InputAdornment>
                    )
                }}
                id="contact-email-intent"
                label="Email"
                placeholder='Your business email'
                name='email'
                required
                type='email'
            />
        </Box>
    )
}
