import {TransitionProps} from "@mui/material/transitions";
import {Slide} from "@mui/material";

export function SlideTransition(props: TransitionProps) {
    return <Slide {...props as any} direction="up"/>;
}
