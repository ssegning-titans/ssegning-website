import React, {memo, useState} from "react";
import {Box, IconButton, Link, ListItemIcon, ListItemText, Menu, MenuItem, Toolbar} from "@mui/material";
import {useRouter} from "next/router";
import LL from 'next/link';
import {ChevronRight, MoreVertical} from "react-feather";
import {HeaderBar} from "@cts/header-bar";

const headerItems = [
    {
        label: 'Home',
        path: '/#home',
    },
    {
        label: 'Project analysis',
        path: '/#project-analysis',
    },
    {
        label: 'Marketing',
        path: '/#marketing',
    },
    {
        label: 'Development',
        path: '/#development',
    },
]


export const Header = memo(() => {
    const {route} = useRouter();

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <HeaderBar>
            <Toolbar component={Box} flexWrap='wrap' paddingX='0 !important'>
                <img src="/logo.png" alt="SSegning Logo" width={32} height={32}/>
                <LL href='/' passHref>
                    <Link variant="h6" color="inherit" noWrap underline='none'
                          sx={{mr: 4, ml: 2, flexGrow: 1}}>SSegning.</Link>
                </LL>

                <Box component='nav' sx={{
                    '& > :not(style) + :not(style)': {
                        ml: 2,
                    },
                    display: {
                        xs: 'none',
                        sm: 'block',
                    }
                }}>
                    {
                        headerItems.map(value => (
                            <LL key={value.path} href={value.path} passHref>
                                <Link underline='none'
                                      color={(value.path !== '/' && route.startsWith(value.path))
                                      || (value.path === '/' && route === '/')
                                          ? 'primary' : 'inherit'}>
                                    {value.label}
                                </Link>
                            </LL>
                        ))
                    }
                </Box>

                <Box component='nav' sx={{
                    '& > :not(style) + :not(style)': {
                        ml: 2,
                    },
                    display: {
                        xs: 'block',
                        sm: 'none'
                    }
                }}>
                    <IconButton
                        id="nav-button"
                        aria-controls="nav-menu"
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                    >
                        <MoreVertical strokeWidth={1}/>
                    </IconButton>
                    <Menu
                        id="nav-menu"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'nav-button',
                        }}
                    >
                        {
                            headerItems.map(value => (
                                <LL key={value.path} href={value.path} passHref>
                                    <MenuItem key={value.path} component='a'>
                                        <ListItemIcon>
                                            <ChevronRight strokeWidth={1}/>
                                        </ListItemIcon>
                                        <ListItemText>{value.label}</ListItemText>
                                    </MenuItem>
                                </LL>
                            ))
                        }
                    </Menu>


                </Box>
            </Toolbar>
        </HeaderBar>
    );
});
Header.displayName = 'Header'
