import '../styles/globals.css';
import type {AppProps} from 'next/app';

import {init} from "@socialgouv/matomo-next";
import {useEffect} from "react";
import {DefaultSeo} from "next-seo";
import SEO from "../next-seo.config";

const MATOMO_URL = process.env.NEXT_PUBLIC_MATOMO_URL;
const MATOMO_SITE_ID = process.env.NEXT_PUBLIC_MATOMO_SITE_ID;

function MyApp({Component, pageProps}: AppProps) {
    useEffect(() => {
        init({
            url: MATOMO_URL!,
            siteId: MATOMO_SITE_ID!
        });
    }, []);

    return (
        <>
            <DefaultSeo {...SEO} />
            <Component {...pageProps} />
        </>
    )
}

export default MyApp
