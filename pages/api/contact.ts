// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type {NextApiRequest, NextApiResponse} from 'next'
import {createTransport} from 'nodemailer';
import Mail from "nodemailer/lib/mailer";

const transporter = createTransport({
    port: Number(process.env.EMAIL_PORT!),
    host: process.env.EMAIL_HOST!,
    auth: {
        user: process.env.EMAIL_USERNAME!,
        pass: process.env.EMAIL_PASSWORD!,
    },
    secure: process.env.EMAIL_SECURE === 'true',
});

const handler = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    try {
        const body = req.body;

        const mailData: Mail.Options = {
            from: {
                name: 'SSegning Website',
                address: 'selastlambou@gmail.com',
            },
            to: [
                {
                    address: 'ssegning@yahoo.com',
                    name: 'SSegningTeam'
                }
            ],
            subject: `[SSegning] Message From ${body.firstName}`,
            text: body.message,
            html: `<div>${JSON.stringify(body, null, 4)}</div>`
        }

        await transporter.sendMail(mailData);

        return res.status(200).json({ok: true});
    } catch (e) {
        console.error(e);
        return res.status(400).json({error: (e as Error).message});
    }

}

export default handler;
