import {Header} from "@cts/header";
import {Footer} from "@cts/footer";
import {NextPage} from "next";
import {Box, Container} from "@mui/material";
import {NextSeo} from "next-seo";
import MuiMarkdown from 'mui-markdown';

import md from '@md/PrivacyPolicy.md';

const PrivacyPolicy: NextPage = () => (
    <>
        <NextSeo title='Privacy Policy' nofollow noindex/>

        <Header/>

        <Box component='main'>
            <Box py='1rem'>
                <Container>
                    <MuiMarkdown>
                        {md}
                    </MuiMarkdown>
                </Container>
            </Box>
        </Box>

        <Footer/>
    </>
);

export default PrivacyPolicy;
