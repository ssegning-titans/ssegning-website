import {Header} from "@cts/header";
import {Footer} from "@cts/footer";
import {NextPage} from "next";
import {Container, Typography} from "@mui/material";
import {NextSeo} from "next-seo";

const NotFound: NextPage = () => (
    <>
        <NextSeo title='404' nofollow noindex/>

        <Header/>

        <main>
            <Container sx={{textAlign: 'center'}}>
                <Typography variant='h3' component='h1'>
                    Ooops!
                </Typography>

                <Typography variant='h1' component='h5'>
                    404
                </Typography>

                <Typography variant='body1' component='p'>
                    The page you're looking for does not exist on this website
                </Typography>
            </Container>
        </main>

        <Footer hideContact/>
    </>
);

export default NotFound;
