import {NextPage} from "next";
import {Header} from "@cts/header";
import {Footer} from "@cts/footer";
import {Box, Container, Grid, Typography} from "@mui/material";
import {ContactForm} from "@cts/contact-form";
import Image from "next/image";
import {NextSeo} from "next-seo";

const sidePanel = [
    {
        i: '/images/development.png'
    },
    {
        i: '/images/marketing.png'
    },
    {
        i: '/images/polygon.png'
    },
    {
        i: '/images/working_dev.png'
    },
]

const random3 = Math.floor(Math.random() * sidePanel.length);

const Contact: NextPage<any> = () => {
    return (
        <>
            <NextSeo title='Contact'/>

            <Header/>

            <main>
                <Container component={Box} id='contact' pt='1rem' pb='2rem'>
                    <Grid container spacing={8} justifyContent="space-between" alignItems='center'>
                        <Grid item xs={12} md={5}>
                            <Typography variant='h2' component='h1' fontWeight='bold' pb='1rem'>
                                Contact
                            </Typography>

                            <Typography variant='body1'>
                                You have an idea and you need to see it coming to life? Then you are at the right point.
                                Just contact us using this field, we will recontact you in 24 hours!
                            </Typography>

                            <ContactForm/>
                        </Grid>

                        <Grid display={{xs: 'none', md: 'block'}} item xs={12} md={6}>
                            <Image src={sidePanel[random3].i} width={1000} height={1000} alt='Contact image'/>
                        </Grid>
                    </Grid>
                </Container>
            </main>

            <Footer hideContact/>
        </>
    )
}

export default Contact;
