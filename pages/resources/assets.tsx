import {Header} from "@cts/header";
import {Footer} from "@cts/footer";
import {NextPage} from "next";
import {
    Avatar,
    Box,
    Container,
    Grid,
    List,
    ListItemAvatar,
    ListItemButton,
    ListItemSecondaryAction,
    ListItemText,
    Typography
} from "@mui/material";
import {NextSeo} from "next-seo";
import Image from "next/image";
import {ArrowDown, Download, Paperclip} from "react-feather";
import {assets} from "@lib/assets";

const Assets: NextPage = () => (
    <>
        <NextSeo title='Assets'/>

        <Header/>

        <Box component='main'>
            <Box bgcolor='#059BFA33' pb='1rem'>
                <Container>
                    <Grid container spacing={8} justifyContent="space-between" alignItems='center'>
                        <Grid item xs={12} md={5}>
                            <Typography variant='h2' component='h1' fontWeight='bold'
                                        pb='1rem'>
                                Need an Asset?
                            </Typography>
                            <Typography variant='body1' component='p' pb='4rem'>
                                Here are all our assets for all our different application and systems. Just pick the one
                                you
                                need. It's free!
                            </Typography>

                            <ArrowDown strokeWidth={1} size={64}/>
                        </Grid>

                        <Grid item xs={12} md={5} display={{xs: 'none', md: 'block'}}>
                            <Image src='/images/folders.png' width={1000} height={1000} alt='Assets image'/>
                        </Grid>
                    </Grid>
                </Container>
            </Box>

            <Box py='1rem'>
                <Container>
                    <Grid container spacing={8} justifyContent="space-between" alignItems='center'>

                        {assets.map(({description, files, name}) => (
                            <Grid item xs={12} md={5}>
                                <Typography variant='h3' pb='1rem'>
                                    {name}
                                </Typography>
                                <Typography variant='body1' component='p'>
                                    {description}
                                </Typography>

                                {files.length === 0 && (
                                    <Typography>No assets here for now</Typography>
                                )}
                                {files.length !== 0 && (
                                    <List>
                                        {files.map(({url, name, mimeType, size}) => (
                                            <ListItemButton component='a' href={url} download={name} title={name}
                                                            color='primary'>
                                                <ListItemAvatar>
                                                    {mimeType.startsWith('image/') ? (
                                                        <Avatar color='primary' src={url} alt={name}/>) : (
                                                        <Avatar color='primary'>
                                                            <Paperclip strokeWidth={1}/>
                                                        </Avatar>
                                                    )}
                                                </ListItemAvatar>

                                                <ListItemText
                                                    primary={name}
                                                    secondary={`${size}`}
                                                />

                                                <ListItemSecondaryAction>
                                                    <Download strokeWidth={1}/>
                                                </ListItemSecondaryAction>
                                            </ListItemButton>
                                        ))}
                                    </List>
                                )}
                            </Grid>
                        ))}

                    </Grid>
                </Container>
            </Box>
        </Box>

        <Footer/>
    </>
);

export default Assets;
