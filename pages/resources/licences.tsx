import {Header} from "@cts/header";
import {Footer} from "@cts/footer";
import {NextPage} from "next";
import {
    Box,
    Container,
    Grid,
    List,
    ListItemAvatar,
    ListItemButton,
    ListItemText,
    Typography,
    useTheme
} from "@mui/material";
import {NextSeo} from "next-seo";
import {ExternalLink, FileText} from "react-feather";
import Image from "next/image";
import {licences} from "@lib/licences";

const Licences: NextPage = () => {
    const {palette} = useTheme();
    return (
        <>
            <NextSeo title='Licences' nofollow noindex/>

            <Header/>

            <Box component='main'>
                <Box bgcolor='#059BFA33' pb='1rem'>
                    <Container>
                        <Grid container spacing={8} justifyContent="space-between" alignItems='center'>
                            <Grid item xs={12} md={5}>
                                <Typography variant='h2' component='h1' fontWeight='bold' pb='1rem'>
                                    Licences
                                </Typography>
                                <Typography variant='body1' component='p' pb='4rem'>
                                    For creating our different application, we use different assets. Those are protected
                                    by
                                    copyrights and different licences. So we've included them here.
                                </Typography>

                                <FileText strokeWidth={1} size={64}/>
                            </Grid>

                            <Grid item xs={12} md={5} display={{xs: 'none', md: 'block'}}>
                                <Image src='/images/diploma.png' width={1000} height={1000} alt='Licence image'/>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>

                <Box py='1rem'>
                    <Container>
                        <List>
                            {licences.map(({url, label, title}) => (
                                <ListItemButton
                                    component='a'
                                    href={url}
                                    target='_blank'
                                    title={title}
                                    color='primary.main'
                                    rel='noopener'
                                >
                                    <ListItemAvatar>
                                        <ExternalLink color={palette.primary.main} strokeWidth={1}/>
                                    </ListItemAvatar>

                                    <ListItemText
                                        primary={label}
                                        secondary={url}
                                    />
                                </ListItemButton>
                            ))}
                        </List>
                    </Container>
                </Box>
            </Box>

            <Footer/>
        </>
    );
};

export default Licences;
