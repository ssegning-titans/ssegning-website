import type {NextPage} from 'next';
import {Footer} from "@cts/footer";
import {Header} from "@cts/header";
import {Box, Container, Grid, Typography} from "@mui/material";
import Image from "next/image";
import {NextSeo} from "next-seo";
import {ContactEmailInput} from "@cts/contact-email-input";

const Home: NextPage = () => {
    return (
        <>
            <NextSeo title='IT Solutions'/>

            <Header/>

            <main>

                <Box>
                    <Box
                        display={{xs: 'none', md: 'block'}}
                        component='img'
                        src='/images/background_header.png'
                        alt='SSegning image'
                        position='absolute'
                        top={0}
                        right={0}
                        zIndex={-1}
                        height='75%'
                    />

                    <Box
                        component='img'
                        src='/images/polygon.png'
                        alt='SSegning image'
                        position='absolute'
                        top='10%'
                        left='10%'
                        zIndex={-1}
                        height='20%'
                    />

                    <Box
                        component='img'
                        src='/images/polygon.png'
                        alt='SSegning image'
                        position='absolute'
                        top='60%'
                        left='-10%'
                        zIndex={-1}
                        height='60%'
                    />
                </Box>

                <Box id='home' py='1rem'>
                    <Container>
                        <Grid container spacing={8} justifyContent="space-between" alignItems='center'
                              minHeight='60vh'>
                            <Grid item xs={12} md={5}>
                                <Typography display={{xs: 'block', md: 'none'}} variant='h3' component='h1'
                                            fontWeight='bold' pb='1rem'>
                                    Hello! We'll help you build your site!
                                </Typography>
                                <Typography display={{xs: 'none', md: 'block'}} variant='h2' component='h1'
                                            fontWeight='bold' pb='1rem'>
                                    We provide personalized IT Solutions
                                </Typography>
                                <Typography variant='body1' component='p' pb='4rem'>
                                    We're a team of developers, managers, designers and hard workers. We love creating
                                    new things, from scratch or not. So we invest them many time in counselling our
                                    customers for them to take control over their fears and to benefit from all their
                                    advantages.
                                </Typography>

                                <ContactEmailInput/>
                            </Grid>

                            <Grid item xs={12} md={6} display={{xs: 'none', md: 'block'}}>
                                <Image src='/images/working_dev.png' width={1000} height={1000} alt='Welcome image'/>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>

                <Box bgcolor='#059BFA33' id='project-analysis' py={4}>
                    <Container>
                        <Grid container spacing={6} justifyContent="space-between" alignItems='center'>
                            <Grid item xs={12} md={6} display={{xs: 'none', md: 'block'}}>
                                <Image src='/images/analyze_large.png' width={1000} height={1000} alt='Analysis image'/>
                            </Grid>

                            <Grid item xs={12} md={5}>
                                <Typography variant='h3' component='h2' pb='1rem'>
                                    We help you analyze your project
                                </Typography>
                                <Typography variant='subtitle1' component='p'>
                                    We guid you through the long process of brainstorming up till your final project.
                                    We help you in dividing your ideas into milestones and document each of them so
                                    it could be understandable for each further team
                                </Typography>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>

                <Box id='marketing' py={6}>
                    <Container>
                        <Grid container spacing={8} justifyContent="space-between" alignItems='center'>
                            <Grid item xs={12} md={5}>
                                <Typography variant='h3' component='h2' pb='1rem'>
                                    Reaching your target audience is now only a matter of time
                                </Typography>
                                <Typography variant='subtitle1' component='p'>
                                    We use all our tools plus open provided tools like Facebook Ads, Google Ads and more
                                    to help you reach your target audience faster and get better results.
                                    For you, we create and edit freely Logos, Flyers, and more.
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={6} display={{xs: 'none', md: 'block'}}>
                                <Image src='/images/marketing.png' width={1000} height={1000} alt='Marketing image'/>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>

                <Box bgcolor='#059BFA33' id='development' py={4}>
                    <Container>
                        <Grid container spacing={8} justifyContent="space-evenly" alignItems='center'>
                            <Grid order={1} item xs={12} md={5}>
                                <Typography variant='h3' component='h2' pb='1rem'>
                                    Development is our tool
                                </Typography>
                                <Typography variant='subtitle1' component='p'>
                                    You may have your requirement, milestones but no team to implement it. That's where
                                    we
                                    come it with our full stack dev-team capable of implementing every single web
                                    development
                                    task. You will find your path here!
                                </Typography>
                            </Grid>

                            <Grid order={0} item xs={12} md={6} display={{xs: 'none', md: 'block'}}>
                                <Image src='/images/development.png' width={1000} height={1000} alt='Development image'/>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>


            </main>

            <Footer/>
        </>
    )
}

export default Home;
